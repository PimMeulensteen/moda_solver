from fds import FD, FDs, str_to_fds
import sys
from typing import List

# 1. Compute a canonical set of FDs F
# 2. Maximize the right-hand sides of the FDs:
#       Replace every functional dependency X→Y∈F by X→X^+  - X
#       So the right-hand side of each functional dependency is the cover of the left-hand side minus the (trivially implies) attributes of the left-hand side.

# 3. Start with the schema S={R}
# 4. Split off violating FD’s one by one:
#   For every R_i ∈ S and X→Y∈F: 
#       if X⊆Ri (that is, X is contained in Ri), 
#       and  Ri⊈X+ (that is, X is not a key of Ri) , 
#       and Y∩Ri≠∅ (that is, Y overlaps with Ri    ),
#           then, let Z=Y∩Ri    
#       and remove Z from Ri, 
#       and add a new relation with attributes XZto S; the key of this relation is X.

def read_input(file: str = "in.txt") -> List[str]:
    """ Read the input from a file. """
    file_read = open(file, "r").read()
    file_read = file_read.split("The minimal keys are:")
    fds = set(filter(None, file_read[0].split("\n")))
    min_keys = set(filter(None, file_read[1].split("\n")))
    min_keys = set(map(frozenset, [filter(lambda y: y not in "\{, \}", x) for x in min_keys]))
    return fds, min_keys


if __name__ == "__main__":
    if len(sys.argv) > 1:
        fds, min_keys = read_input(sys.argv[1])
    else:
        print(f"Correct sytax is: python3 {sys.argv[0]} [FILE1.txt]")
        print(f"For example: python3 {sys.argv[0]} my_input.txt")
        sys.exit(1)

    fd_obj=FDs(str_to_fds(fds))
    print(f"In BCNF?: {fd_obj.is_bcnf(min_keys)}")
    print(f"In 3NF?: {fd_obj.is_3nf(min_keys)}")