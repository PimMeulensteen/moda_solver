from fds import FD, FDs, str_to_fds
import sys
from typing import List


def read_input(file: str = "in.txt") -> List[str]:
    """ Read the input from a file. """
    file_read = open(file, "r").read()
    file_read = file_read.split("The minimal keys are:")
    fds = set(filter(None, file_read[0].split("\n")))
    min_keys = set(filter(None, file_read[1].split("\n")))
    min_keys = set(
        map(
            frozenset,
            [filter(lambda y: y not in "\{, \}", x) for x in min_keys],
        )
    )
    return fds, min_keys


if __name__ == "__main__":
    if len(sys.argv) > 1:
        fds, min_keys = read_input(sys.argv[1])
    else:
        print(f"Correct sytax is: python3 {sys.argv[0]} [FILE1.txt]")
        print(f"For example: python3 {sys.argv[0]} my_input.txt")
        sys.exit(1)

    fd_obj = FDs(str_to_fds(fds))
    print(f"In BCNF?: {fd_obj.is_bcnf(min_keys)}")
    print(f"In 3NF?: {fd_obj.is_3nf(min_keys)}")